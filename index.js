const express = require('express')
const PubSub = require('pubsub-js');
const data = require('./moduleledger');
import bip39 from './bip39english'
var mySubscriber = function (msg, data) {
    console.log(msg);
};
var token = PubSub.subscribe('LedgerEntry', mySubscriber);
const webserver = express()
    .use((req, res) =>
        res.sendFile('/websocket-client.html', { root: __dirname })
    )
    .listen(3000, () => console.log(`Listening on ${3000}`))

const { WebSocketServer } = require('ws')
const sockserver = new WebSocketServer({ port: 443 })
sockserver.on('connection', ws => {
    console.log('New client connected!')
    ws.send('connection established');
    var mySubscriber = function (msg, data) {
        console.log('msg'+msg);
        sockserver.clients.forEach(client => {
            console.log( )
            client.send(sockserver.clients.forEach(client => {
                console.log(`distributing message: ${data}`)
                client.send(data)
            }))
        })
    };
    var token = PubSub.subscribe('LedgerHash', mySubscriber);

    ws.on('close', () => console.log('Client has disconnected!'))
    ws.on('message', data => {
        sockserver.clients.forEach(client => {
            console.log(`distributing message: ${data}`)
            client.send(`${data}`)
        })
    })
    ws.onerror = function () {
        console.log('websocket error')
    }
})
