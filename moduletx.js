const   api =require('ripple-binary-codec');
const xrpl = require('xrpl');
var fs = require('fs');
const PubSub = require('pubsub-js');
const data = [];
module.exports = sleepySays;
function sleepySays (input, isSleepySleeping) {
    if (isSleepySleeping) {
        return '(◡ ‿ ◡ ✿) zZZz'
    }
    return `( o ‿ ~ ✿) ${input}`
}

async function mintNFT(wallet,client) {
    const transactionBlob = {
        "TransactionType": "NFTokenMint",
        "Account": wallet.classicAddress,
        "URI": xrpl.convertStringToHex("ipfs://bafybeigdyrzt5sfp7udm7hu76uh7y26nf4dfuylqabf3oclgtqy55fbzdi"),
        "Flags": parseInt('1'),
        "TransferFee": 20,
        "NFTokenTaxon": 0
    }
    let nfttokemint= {
        "TransactionType": "NFTokenMint",
        "Account": wallet.classicAddress,
        "TransferFee": 314,
        "NFTokenTaxon": 77,
        "Flags": 8,
        "Fee": "10",
        "URI": "697066733A2F2F62616679626569676479727A74357366703775646D37687537367568377932366E6634646675796C71616266336F636C67747179353566627A6469",
        "Memos": [
            {
                "Memo": {
                    "MemoType":
                        "687474703A2F2F6578616D706C652E636F6D2F6D656D6F2F67656E65726963",
                    "MemoData": "72656E74"
                }
            }
        ]
    }

        let tx = await client.submit(nfttokemint, { wallet: wallet} );
    //console.log('MINT2:'+JSON.stringify(tx));
    //console.log(tx.Account)

    let info = await client.request({
        method: "account_info",
        account: wallet.classicAddress
    });
   // console.log('info: ' + JSON.stringify(info, null, 2));
        let nfts = await client.request({
            method: "account_nfts",
            account: wallet.classicAddress
        });
  //console.log('account nft count;' +nfts.result.account_nfts.length);


        // ------------------------------------------------------- Report results

      let results = 'nfts: ' + JSON.stringify(tx, null, 2)
    PubSub.publish('Transaction', tx.hash);

    // ----------------------------------------------------- Submit signed blob
    const tr = await client.submit(transactionBlob, { wallet: wallet} )
     nfts = await client.request({
        method: "account_nfts",
        account: wallet.classicAddress
    })

}
async function getledgerdata(  hash, client) {

    let req = {
        "id": Math.floor(Math.random() * 2048),
        "ledger_hash": hash,
        "command": "ledger_data",
        "limit": 5,
        "binary": true
    }
    let res = await client.request(req);
    const ld = res.result;



    //for (let idx = 0; idx < res.result.state.length; idx++) {
        //let obj = api.decode(res.result.state[idx].data);


    //}
    for (let idx = 0; idx < res.result.state.length; idx++) {
        ///let obj = api.decode(res.result.state[idx].data);

        //PubSub.publish('LedgerEntry', JSON.stringify(obj));
      //  PubSub.publishSync(JSON.stringify(obj));




    }}
async function jimmyjohn(){

    let PUBLIC_SERVER = 'wss://testnet.xrpl-labs.com/'

    console.log(PUBLIC_SERVER);

    const client = await new xrpl.Client(PUBLIC_SERVER);

    await client.connect();
    const fund_result = await client.fundWallet()
    const test_wallet = fund_result.wallet
    console.log('FUNDWALLET'+JSON.stringify(test_wallet, null, 4));
    client.on( 'transaction', async (ledger) => {
        console.log( ' 7777777777777'+ JSON.stringify(ledger.engine_result));
       console.log( ' 7777777777777'+ ledger.transaction.hash);
       PubSub.publish('TXN', ledger.transaction.hash);
        console.log( ' 88888888888888');

        //PubSub.publish();
       //console.log(' ledger_ledgerindexe'+ JSON.stringify(ledger ));
        //console.log( ' hash'+ );

       //fs.appendFileSync('./testOutput.json', JSON.stringify(ledger,null,2));


        await mintNFT(test_wallet, client);
        //let ed =await getledgerdata(ledger.ledger_hash, client);

    });

    let res = await   client.request({
        command: 'subscribe',
        streams: ['transactions']})
    console.log('jimmy2john');

}
setTimeout(async () =>{

    await jimmyjohn();
}, 1000);

module.exports = sleepySays
